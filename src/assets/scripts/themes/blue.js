var blueTheme = {
    BASE: {
        'text-align': 'left',
        'color': '#3f3f3f',
        'line-height': '1.5'
    },
    BASE_BLOCK: {
        'margin': '20px 10px'
    },
    // block element
    block: {
        h2: {
            'text-align': 'center',
            'color': '#3f3f3f',
            'line-height': '1.75',
            'font-size': '1.2em',
            'font-weight': 'bold',
            'display': 'table',
            'margin': '2em auto 1em auto',
            'padding': '0 1em',
            'border-bottom': '1px solid #007dff',
        },
        h3: {
            'font-weight': 'bold',
            'font-size': '120%',
            'margin': '40px 10px 20px 10px',
            'text-align': 'left',
            'color': '#3f3f3f',
            'line-height': '1.75',
            'padding-left': '8px',
            'border-left': '4px solid #007dff'
        },
        p: {
            'margin': '10px 10px',
            'line-height': '1.6'
        },
        blockquote: {
            'color': 'rgb(91, 91, 91)',
            'padding': '1px 0 1px 10px',
            'background': 'rgba(158, 158, 158, 0.1)',
            'border-left': '3px solid #68b3ff',
        },
        code: {
            'font-size': '80%',
            'overflow': 'auto',
            'color': '#333',
            'background': '#ecf1f8',
            'border-radius': '2px',
            'padding': '10px',
            'line-height': '1.3',
            'border': '1px solid rgb(236,236,236)',
            'margin': '20px 0',
        },
        image: {
            'border-radius': '4px',
            'display': 'block',
            'margin': '20px auto',
            'width': '100%',
        },
        image_org: {
            'border-radius': '4px',
            'display': 'block',
        },
        ol: {
            'margin-left': '0',
            'padding-left': '20px'
        },
        ul: {
            'margin-left': '0',
            'padding-left': '20px',
            'list-style': 'circle',
        },
        footnotes: {
            'margin': '10px 10px',
            'font-size': '14px'
        }
    },
    inline: {
        // inline element
        listitem: {
            'text-indent': '-20px',
            'display': 'block',
            'margin': '10px 10px',
        },
        codespan: {
            'font-size': '90%',
            // 'font-family': FONT_FAMILY_MONO,
            'color': '#007dff',
            'background': '#ecf1f8',
            'padding': '3px 5px',
            'border-radius': '2px',
        },
        link: {
            'color': '#007dff'
        },
        strong: {
            'color': '#007dff'
        },
        table: {
            'border-collapse': 'collapse',
            'margin': '20px 0',
        },
        thead: {
            'background': 'rgba(0,0,0,0.05)',
        },
        td: {
            'font-size': '80%',
            'border': '1px solid #dfdfdf',
            'padding': '4px 8px',
        },
        footnote: {
            'font-size': '12px',
        }
    }
}