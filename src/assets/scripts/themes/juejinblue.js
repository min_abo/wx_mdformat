var juejinblueTheme = {
    BASE: {
        'text-align': 'left',
        'line-height': '1.5',
    },
    BASE_BLOCK: {
        'margin': '20px 10px',
    },
    // block element
    block: {
        div: {
            'color': '#595959',
        'font-size': '15px',
        'font-family': '-apple-system,system-ui,BlinkMacSystemFont,Helvetica Neue,PingFang SC,Hiragino Sans GB,Microsoft YaHei,Arial,sans-serif',
        'background-image': 'linear-gradient(90deg,rgba(60,10,30,.04) 3%,transparent 0),linear-gradient(1turn,rgba(60,10,30,.04) 3%,transparent 0)',
        'background-size': '20px 20px',
        'background-position': '50%',
        },
        h1: {
            'position': 'absolute',
            'content': '',
            'top': '-10px',
            'left': '50%',
            'width': '32px',
            'height': '32px',
            'transform': 'translateX(-50%)',
            'background-size': '100% 100%',
            'opacity': '.36',
            'background-repeat': 'no-repeat',
            'background': 'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAABfVBMVEX///8Ad/8AgP8AgP8AgP8Aff8AgP8Af/8AgP8AVf8Af/8Af/8AgP8AgP8Af/8Afv8AAP8Afv8Afv8Aef8AgP8AdP8Afv8AgP8AgP8Acf8Ae/8AgP8Af/8AgP8Af/8Af/8AfP8Afv8AgP8Af/8Af/8Afv8Afv8AgP8Afv8AgP8Af/8Af/8AgP8AgP8Afv8AgP8Af/8AgP8AgP8AgP8Ae/8Afv8Af/8AgP8Af/8AgP8Af/8Af/8Aff8Af/8Abf8AgP8Af/8AgP8Af/8Af/8Afv8AgP8AgP8Afv8Afv8AgP8Af/8Aff8AgP8Afv8AgP8Aff8AgP8AfP8AgP8Ae/8AgP8Af/8AgP8AgP8AgP8Afv8AgP8AgP8AgP8Afv8AgP8AgP8AgP8AgP8AgP8Af/8AgP8Af/8Af/8Aev8Af/8AgP8Aff8Afv8AgP8AgP8AgP8Af/8AgP8Af/8Af/8AgP8Afv8AgP8AgP8AgP8AgP8Af/8AeP8Af/8Af/8Af//////rzEHnAAAAfXRSTlMAD7CCAivatxIDx5EMrP19AXdLEwgLR+6iCR/M0yLRzyFF7JupSXn8cw6v60Q0QeqzKtgeG237HMne850/6Qeq7QaZ+WdydHtj+OM3qENCMRYl1B3K2U7wnlWE/mhlirjkODa9FN/BF7/iNV/2kASNZpX1Wlf03C4stRGxgUPclqoAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4gEaBzgZ4yeM3AAAAT9JREFUOMvNUldbwkAQvCAqsSBoABE7asSOBRUVVBQNNuy9996789+9cMFAMHnVebmdm+/bmdtbQv4dOFOW2UjPzgFyLfo6nweKfIMOBYWwFtmMPGz2Yj2pJI0JDq3udJW6VVbmKa9I192VQFV1ktXUAl5NB0cd4KpnORqsEO2ZIRpF9gJfE9Dckqq0KuZt7UAH5+8EPF3spjsRpCeQNO/tA/qDwIDA+OCQbBoKA8NOdjMySgcZGVM6jwcgRuUiSs0nlPFNSrEpJfU0jTLD6llqbvKxei7OzvkFNQohi0vAsj81+MoqsCaoPOQFgus/1LyxichW+hS2JWCHZ7VlF9jb187pIAYcHiViHAMnp5mTjJ8B5xeEXF4B1ze/fTh/C0h398DDI9HB07O8ci+vRBdvdGnfP4gBuM8vw7X/G3wDmFhFZEdxzjMAAAAldEVYdGRhdGU6Y3JlYXRlADIwMTgtMDEtMjZUMDc6NTY6MjUrMDE6MDA67pVWAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE4LTAxLTI2VDA3OjU2OjI1KzAxOjAwS7Mt6gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAAWdEVYdFRpdGxlAGp1ZWppbl9sb2dvIGNvcHlxapmKAAAAV3pUWHRSYXcgcHJvZmlsZSB0eXBlIGlwdGMAAHic4/IMCHFWKCjKT8vMSeVSAAMjCy5jCxMjE0uTFAMTIESANMNkAyOzVCDL2NTIxMzEHMQHy4BIoEouAOoXEXTyQjWVAAAAAElFTkSuQmCC)'
        },
        h2: {
            'text-align': 'center',
            'color': '#3f3f3f',
            'line-height': '1.75',
            'font-size': '1.2em',
            'font-weight': 'bold',
            'display': 'table',
            'margin': '2em auto 1em auto',
            'padding': '0 1em',
            'border-bottom': '1px solid #007dff',
        },
        h3: {
            'font-weight': 'bold',
            'font-size': '120%',
            'margin': '40px 10px 20px 10px',
            'text-align': 'left',
            'color': '#3f3f3f',
            'line-height': '1.75',
            'padding-left': '8px',
            'border-left': '4px solid #007dff'
        },
        p: {
            'margin': '10px 10px',
            'line-height': '1.6'
        },
        blockquote: {
            'color': 'rgb(91, 91, 91)',
            'padding': '1px 0 1px 10px',
            'background': 'rgba(158, 158, 158, 0.1)',
            'border-left': '3px solid #68b3ff',
        },
        code: {
            'font-size': '80%',
            'overflow': 'auto',
            'color': '#333',
            'background': '#ecf1f8',
            'border-radius': '2px',
            'padding': '10px',
            'line-height': '1.3',
            'border': '1px solid rgb(236,236,236)',
            'margin': '20px 0',
        },
        image: {
            'border-radius': '4px',
            'display': 'block',
            'margin': '20px auto',
            'width': '100%',
        },
        image_org: {
            'border-radius': '4px',
            'display': 'block',
        },
        ol: {
            'margin-left': '0',
            'padding-left': '20px'
        },
        ul: {
            'margin-left': '0',
            'padding-left': '20px',
            'list-style': 'circle',
        },
        footnotes: {
            'margin': '10px 10px',
            'font-size': '14px'
        }
    },
    inline: {
        // inline element
        listitem: {
            'text-indent': '-20px',
            'display': 'block',
            'margin': '10px 10px',
        },
        codespan: {
            'font-size': '90%',
            // 'font-family': FONT_FAMILY_MONO,
            'color': '#007dff',
            'background': '#ecf1f8',
            'padding': '3px 5px',
            'border-radius': '2px',
        },
        link: {
            'color': '#007dff'
        },
        strong: {
            'color': '#007dff'
        },
        table: {
            'border-collapse': 'collapse',
            'margin': '20px 0',
        },
        thead: {
            'background': 'rgba(0,0,0,0.05)',
        },
        td: {
            'font-size': '80%',
            'border': '1px solid #dfdfdf',
            'padding': '4px 8px',
        },
        footnote: {
            'font-size': '12px',
        }
    }
}